module.exports = function (app) {
	app.use('/auth', require('./auth'));
	app.use('/api/profile', require('./profile'));
	app.use('/api/job', require('./job'));
	

};