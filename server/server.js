
var express = require('express');
var mongoose = require('mongoose');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var path = require('path');
var config = require('./config');
var fileUpload = require('express-fileupload');


mongoose.set('debug', true);
mongoose.connection.on('error', function (err) {
	console.error('MongoDB error: %s', err);
	process.exit(1);
});

var connectionString = config.mongo.connectionstring;

console.log("connection string : " + connectionString);

mongoose.connect(connectionString);



var app = express();
var server = require('http').createServer(app);

app.set('case sensitive routing', false)

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(compression());
app.use(fileUpload());

app.use(express.static(__dirname + '/../client'));
app.use('/public', express.static(__dirname + '/public'));

//Requiring routes, make sure middleware is executed beforehand
require('./routes')(app);

//var port = process.env.PORT;
var port = config.port;
var server = app.listen(port, function () {
	var host = server.address().address;
	var port = server.address().port;
	console.log("host " + host);
	console.log('Express Server listening at http://%s:%s in %s mode', host, port, app.get('env'))
});

// Expose app
exports = module.exports = app;