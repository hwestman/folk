'use strict';
var _ = require('lodash');
var async = require('async');
var Profile = require('../datasets/profile.model');
var Application = require('../datasets/application.model');
var Job = require('../datasets/job.model');
var User = require('../auth/user.model');
var path = require('path');
var fs = require('fs');

var appdir = path.dirname(require.main.filename) + '/';
var PICTUREPATH = 'public/profilepictures/';


exports.getProfile = function (req, res) {
	let userId = req.user;
	if(req.query.profileId != null){
		userId = req.query.profileId;
	}
	
	Profile.findOne({ userId: userId }).exec().then(function (profile) {
		

		if (!profile) {
			return res.status(400).send({ message: "No profile found" });
		}

		fs.stat(appdir+profile.picture, function (err, stat) {
			if (err != null) {
				profile.picture = '';
			}
			
			return res.status(200).json(profile);
		});

		
	})
		.catch(function (err) {
			console.log('error:', err);
			return res.status(400).send({ message: err });
		});
}

exports.updateProfile = function (req, res) {

	var updatedProfile = JSON.parse(req.body.profile);

	if (req.files != null) {
		var profilePicture = req.files.file;
		updatedProfile.picture = PICTUREPATH + req.user + '.png';

		var mvTo = appdir + updatedProfile.picture;

		profilePicture.mv(mvTo, function (err) {
			if (err) {
				console.log("could not save image");
				//res.status(500).send(err);
			}
			else {
				//res.send('File uploaded!');
				console.log("image was saved");
			}
		});
	}

	Profile.findOne({ userId: req.user }).exec().then(function (profile) {


		if (!profile) {
			return res.status(400).send({ message: "No profile found" });
		}

		//var updatedProfile = req.body;
		var updated = _.merge(profile, updatedProfile);
		
		return updated.save();

	})
		.then(function (profile) {

			
			return res.status(200).json(profile);
		})
		.catch(function (err) {
			console.log('error:', err);
			return res.status(400).send({ message: err });

		});

};
exports.getApplications = function (req, res) {

	Profile.findOne({ userId: req.user },function (err,profile) {


		if (!profile) {
			return res.status(400).send({ message: "No profile found" });
		}
		let pro = profile._id.toString();
	
		Application.find({ profileId: pro},function(err,applications) {
 
			if (!applications || applications.length<=0) {
				return res.status(400).send({ message: "No profile found" });
			}
			let applicationsDTO = [];
			async.eachSeries(applications, function (application, callback) {
				let applicationDTO = application.toObject();

				Job.findOne({ _id: application.jobId }).exec().then(function (job) {
					
					applicationDTO.job = job.toObject();  
					
					Profile.findOne({ _id: applicationDTO.profileId }, function (err, applicant) {
						applicationDTO.userProfile = applicant.toObject();  
						applicationsDTO.push(applicationDTO);						
						callback(err)
					});
				});
			}, function (err) {
				if (err) throw err;				
				//return res.status(200).json({ job: job, applications: applicationsDTO });
				return res.status(200).json({applications:applicationsDTO});
			});

		});
	});
};
