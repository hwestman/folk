var express = require('express');
var profileController = require('./profile.controller');
var authUtils = require('../auth/auth-utils');
//var  multer = require('multer');


var router = express.Router();

// var uploading = multer({
//     dest: __dirname + 'uploads/',

// })

router.use(authUtils.ensureAuthenticated); //auth only appied for following paths, not the paths above
router.get('/', profileController.getProfile);
router.get('/applications', profileController.getApplications);
//router.post('/update',uploading.single('profilePicture'), profileController.updateProfile );
router.post('/update', profileController.updateProfile);
router.put('/', profileController.updateProfile);



module.exports = router;