var express = require('express');
var authUtils = require('./auth-utils');
var authController = require('./auth.controller.js');
var meController = require('./me.controller.js');
var google = require('./google.js');
// var linkedin = require('./linkedin.js');
var router = express.Router();

router.post('/signup', authController.signup);
router.post('/login', authController.login);

router.post('/google', google.authenticate);
// router.post('/linkedin', linkedin.authenticate);


router.use(authUtils.ensureAuthenticated); //auth only appied for following paths, not the paths above
router.get('/me', meController.getMe);
router.put('/me', meController.updateMe);

module.exports = router;