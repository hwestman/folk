var _ = require('lodash');
var Entity = require('./user.model.js');
var Profile = require('../datasets/profile.model.js');
var jwt = require('jwt-simple');
var authUtils = require('./auth-utils');
exports.signup = function (req, res) {
	console.log("req body " + req.body.email);

	Entity.findOne({ email: req.body.email }, function (err, existingUser) {

		if (existingUser) {
			return res.status(409).send("Email is already taken");
		}

		var user = new Entity({
			email: req.body.email,
			password: req.body.password
		});
		user.save(function (err) {
			if (err) {
				console.log(err)
			}
			var values = req.body.displayName.split('_');
			var firstName = "";
			var lastName = "";
			if (values[0] != null) {
				firstName = values[0];
			}
			if (values[1] != null) {
				lastName = values[1];
			}
			var userProfile = new Profile({
				userId: user._id,
				firstname: firstName,
				lastname: lastName


			});

			userProfile.save(function (err) {
				if (err) {
					console.log(err)
				}

				return res.status(201).json({ token: authUtils.createJWT(user) });
			});

		});

	});
};

exports.login = function (req, res) {
	
	Entity.findOne({ email: req.body.email }, '+password', function (err, user) {
		if (!user) {
			return res.status(401).json({ message: 'Wrong email and/or password' });
		}
		user.comparePassword(req.body.password, function (err, isMatch) {
			if (!isMatch) {
				return res.status(401).send({ message: 'Wrong email and/or password' });
			}
			res.send({ token: authUtils.createJWT(user) });
		});
	});
};