'use strict';
var _ = require('lodash');
var async = require('async');
var Job = require('../datasets/job.model');
var Profile = require('../datasets/profile.model');
var Application = require('../datasets/application.model');
var path = require('path');

var appdir = path.dirname(require.main.filename) + '/';
var RESUMEPATH = 'public/resumes/';

/**
 * Takes a jobid and gets the jobobject
 * This is middleware used to get jobobject, so as to not have to do that in every function that needs it
 */
exports.getJobObject = function (req, res, next) {

	var dto;

	if (req.method == "GET") {
		dto = req.query;
	} else if (req.method == "POST") {
		dto = req.body;
	} else {
		return res.status(400).send({ message: "Cant run that middleware on a " + req.method + " methord" });
	}

	if (dto == null || dto.jobId == null) {
		next();
	} else {
		Job.findOne({ _id: dto.jobId }).exec().then(function (job) {
			if (!job) {
				return res.status(400).send({ message: "No jobs found" });
			}

			req.job = job;
			next();

		}).catch(function (err) {
			console.log('error:', err);
			return res.status(400).send({ message: err });
		});
	}
}

/**
 * Takes a jobid
 * Gets a specific job
 */
exports.getJob = function (req, res) {

	var job = req.job;

	Profile.findOne({ _id: job.manager }, function (err, manager) {

		job.manager = {
			id:manager.userId,
			firstname: manager.firstname,
			lastname: manager.lastname,
			phone: manager.phone,
			email: manager.email,
			role: manager.role
		};

		Profile.findOne({ _id: job.recruiter }, function (err, recruiter) {

			job.recruiter = {
				id:recruiter.userId,
				firstname: recruiter.firstname,
				lastname: recruiter.lastname,
				phone: recruiter.phone,
				email: recruiter.email,
				role: recruiter.role
			};

			Application.find({ jobId: job._id }, function (err, applications) {

				if (!applications) {
					return res.status(200).json({ job: job });
				}
				let applicationsDTO = [];
				async.eachSeries(applications, function (application, callback) {
					let applicationDTO = application.toObject();
					Profile.findOne({ _id: applicationDTO.profileId }, function (err, applicant) {
						applicationDTO.userProfile = applicant.toObject();  
						applicationsDTO.push(applicationDTO);						
						callback(err)
					});
				}, function (err) {
					if (err) throw err;
					

					return res.status(200).json({ job: job, applications: applicationsDTO });
				});
			});
		});
	});
};

/**
 * takes no parameter
 * Gets data needed to create a job, eg. recruiter, manager
 */
exports.getCreateData = function (req, res) {


	Profile.find({ role: 'admin' }).exec().then(function (profiles) {

		if (!profiles) {
			return res.status(400).send({ message: "No profiles found" });
		}


		return res.status(200).json(profiles);
	}).catch(function (err) {
		console.log('error:', err);
		return res.status(400).send({ message: err });
	});


}
/**
 * takes a jobid
 * Works the same as getcreatedata, only it gets all data registered for that specific job
 */
exports.getUpdateData = function (req, res) {

	var job = req.job;

	Profile.findOne({ _id: job.manager }, function (err, manager) {
		if (!manager) {
			return res.status(400).send({ message: "No recruiters found" });
		}
		job.manager = {
			id: manager._id,
			firstname: manager.firstname,
			lastname: manager.lastname,
			phone: manager.phone,
			email: manager.email,
			role: manager.role
		};

		Profile.findOne({ _id: job.recruiter }, function (err, recruiter) {
			if (!recruiter) {
				return res.status(400).send({ message: "No recruiters found" });
			}
			job.recruiter = {
				id: recruiter._id,
				firstname: recruiter.firstname,
				lastname: recruiter.lastname,
				phone: recruiter.phone,
				email: recruiter.email,
				role: recruiter.role
			};

			Profile.find({ role: 'admin' }).exec().then(function (profiles) {

				if (!profiles) {
					return res.status(400).send({ message: "No profiles found" });
				}
				var result = {
					job: job,
					managers: profiles,
					recruiters: profiles
				}

				return res.status(200).json(result);
			}).catch(function (err) {
				console.log('error:', err);
				return res.status(400).send({ message: err });
			});
		});

	}).catch(function (err) {
		console.log('error:', err);
		return res.status(400).send({ message: err });
	});
}
/**
 * Takes a job object 
 * and creates that job
 */
exports.createJob = function (req, res) {

	var jobDTO = req.body;

	if (jobDTO.manager == null || jobDTO.recruiter == null) {
		return res.status(400).send("Manager and recruiter must be set");
	}

	var job = new Job({
		title: req.body.title,
		deadline: req.body.deadline,
		manager: req.body.manager._id,
		recruiter: req.body.recruiter._id,
		description: req.body.description,
		location: req.body.location
	});
	job.save(function (err) {
		if (err) {
			console.log(err)
			return res.status(400);
		}

		return res.status(200).send();

	}).catch(function (err) {
		console.log('error:', err);
		return res.status(400).send({ message: err });
	});

}
/**
 * takes a job object 
 * and updates that specific job
 */
exports.updateJob = function (req, res) {

	var updatedJob = {
		title: req.body.title,
		deadline: req.body.deadline,
		manager: req.body.manager._id,
		recruiter: req.body.recruiter._id,
		description: req.body.description,
		location: req.body.location
	};

	var job = req.job;

	var updated = _.merge(job, updatedJob);

	updated.save()
		.then(function (job) {
			return res.status(200).json(job);
		})
		.catch(function (err) {
			console.log('error:', err);
			return res.status(400).send({ message: err });

		});
}

/**
 * takes a jobid 
 * and deletes that specific job.
 * TODO: Set deleteflag and dont actually delete it.
 */
exports.deleteJob = function (req, res) {


	req.job.remove()
		.then(function (job) {

			return res.status(200).json(job);
		})
		.catch(function (err) {
			console.log('error:', err);
			return res.status(400).send({ message: err });

		});

}

/**
 * takes no parameter
 * Gets all jobs
 */
exports.getJobs = function (req, res) {

	Job.find().exec().then(function (jobs) {

		if (jobs.length <= 0) {
			return res.status(400).send({ message: "No jobs found" });
		}

		async.eachSeries(jobs, function (job, callback) {
			
			Profile.findOne({ _id: job.manager }, function (err, manager) {

				job.manager = {
					firstname: manager.firstname,
					lastname: manager.lastname,
					phone: manager.phone,
					email: manager.email,
					role: manager.role
				};
				callback(err)
			});
		}, function (err) {
			if (err) throw err;
			async.eachSeries(jobs, function (job, callback) {
				Profile.findOne({ _id: job.recruiter }, function (err, recruiter) {

					job.recruiter = {
						firstname: recruiter.firstname,
						lastname: recruiter.lastname,
						phone: recruiter.phone,
						email: recruiter.email,
						role: recruiter.role
					};

					var p1 = new Promise(function (resolve, reject) {
						
						Application.find({ jobId: job._id }, function (err, applications) {

							job.applied = applications.length;
							resolve();
						});
					});

					p1.then(function (res) {

						callback(err)
					});
				});
			}, function (err) {
				if (err) throw err;
				
				return res.status(200).json(jobs);
			});
			////////////////


		});

		//return res.status(400).json({message:"Did not run callback:("});

	}).catch(function (err) {
		console.log('error:', err);
		return res.status(400).send({ message: err });
	});

};

/**
 * Takes a job id and an applicationobject 
 * and creates an application for that job
 */
exports.submitApplication = function (req, res) {

	var applicationDTO = JSON.parse(req.body.application);
	Application.find({ user: applicationDTO.profileId, jobId: applicationDTO.jobId }, function (err, applications) {

		if (applications.length > 0) {
			return res.status(409).json({ message: 'User has already applied' });
		}

		if (req.files != null) {

			var resume = req.files.file;
			applicationDTO.cvFilename = resume.name;
			applicationDTO.cvFilepath = RESUMEPATH + req.user + applicationDTO.cvFilename;

			var mvTo = appdir + applicationDTO.cvFilepath;

			resume.mv(mvTo, function (err) {
				if (err) {
					console.log(err);
					console.log("could not save resume");
					//res.status(500).send(err);
				}
				else {
					//res.send('File uploaded!');
					console.log("resume was saved");
				}
			});
		}

		var application = new Application({
			applicationText: applicationDTO.applicationtext,
			cvFilepath: applicationDTO.cvFilepath,
			cvFilename: applicationDTO.cvFilename,
			jobId: applicationDTO.jobId,
			profileId: applicationDTO.profileId
		});
		application.save(function (err) {
			if (err) {
				console.log(err)
				return res.status(400);
			}

			return res.status(200).send();

		});

	});

}