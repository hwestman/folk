var express = require('express');
var jobController = require('./job.controller');
var authUtils = require('../auth/auth-utils');
//var  multer = require('multer');


var router = express.Router();


//populate job object from id
router.use(jobController.getJobObject);
router.use(authUtils.ensureAuthenticated);

router.get('/getJobs', jobController.getJobs);
router.get('/job', jobController.getJob);

//auth only appied for following paths, not the paths above


router.post('/getUpdateData', jobController.getUpdateData);
router.post('/update', jobController.updateJob);
router.post('/delete', jobController.deleteJob);
router.get('/getCreateData', jobController.getCreateData);
router.post('/create', jobController.createJob);
router.post('/submitApplication', jobController.submitApplication);

module.exports = router;