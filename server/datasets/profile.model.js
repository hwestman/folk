var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var schema = new Schema({
	userId: { type: String, unique: true },
	email: { type: String },
	picture: String,
	firstname: String,
	lastname: String,
	phone: Number,
	address: String,
	postcode: Number,
	city: String,
	gender: { type: String, enum: ['male', 'female', 'other', 'noneOfYourBusiness'] },
	image: String,
	employmentstate: String,
	role: String,
	registerDate: { type: Date, default: Date.now },
	manager: String,
	emergencyContact: String,
	emergencyContactNumber: String,
	salary: Number,
	position: String,
	tfn: String,
	superDetails: String

});

module.exports = mongoose.model('Profile', schema);