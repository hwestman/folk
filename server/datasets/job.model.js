var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var schema = new Schema({
	title: String,
	location: String,
	description: String,
	manager: Schema.Types.Mixed,
	recruiter: Schema.Types.Mixed,
	applied: String,
	deadline: Schema.Types.Mixed,
	lastEdited: { type: Date },
	createdBy: String,
	createDate: { type: Date, default: Date.now }
});

module.exports = mongoose.model('Job', schema);