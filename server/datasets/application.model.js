var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var schema = new Schema({
	createDate: { type: Date, default: Date.now },
	applicationText: String,
	cvFilepath: String,
	cvFilename: String,
	jobId: String,
	profileId: Schema.Types.Mixed,
});

module.exports = mongoose.model('Application', schema);