import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';
import { DialogController } from 'aurelia-dialog';


// import {pdf} from "pdfjs-dist";
import * as pdfj from 'pdfjs-dist';
import * as toastr from 'toastr';
@inject(HttpClient, Router, DialogController, json)
export class ViewApplication {

	pdfDoc = null;
	pageNum = 1;
	pageRendering = false;
	pageNumPending = null;
	scale = 1;
	canvas = null;
	ctx = null;


	constructor(http, router, dialogController) {
		this.dialogController = dialogController;
		this.router = router;
		this.http = http;
		this.toastr = toastr;
		//this.toastr.options.positionClass = 'toast-top-center';
	}
	activate(dto) {
		
		this.index = dto.index;
		this.applications = dto.applications;
		this.application = dto.applications[dto.index];

		this.getPDF();
	}

	getNextApplication(index) {
		if (index > this.applications.length - 1) {
			index = 0;
			this.toastr.info('Back to first application');
		}
		if (index < 0) {
			index = this.applications.length - 1;
		}
		this.application = this.applications[index];
		this.index = index;

		this.getPDF();
	}
	close() {
		this.dialogController.ok();
	}
	getPDF() {
		try {
			pdfj.PDFJS.workerSrc = 'jspm_packages/npm/pdfjs-dist@1.6.274/build/pdf.worker.js';

			pdfj.PDFJS.getDocument(this.application.cvFilepath).then(pdfDoc_ => {
				this.pdfDoc = pdfDoc_;
				this.numPages = this.pdfDoc.numPages;
				//document.getElementById('page_count').textContent = pdfDoc.numPages;

				// Initial/first page rendering
				this.renderPage(this.pageNum);
			});
		} catch (err) {
			this.toastr.error('Could not load pdf, contact support');
			if (this.ctx != null) {
				this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
			}
		}
	}
	onNextPage() {
		if (this.pageNum >= this.pdfDoc.numPages) {
			return;
		}
		this.pageNum++;
		this.queueRenderPage(this.pageNum);
	}
	onPrevPage() {
		if (this.pageNum <= 1) {
			return;
		}
		this.pageNum--;
		this.queueRenderPage(this.pageNum);
	}

	queueRenderPage(num) {
		if (this.pageRendering) {
			this.pageNumPending = num;
		} else {
			this.renderPage(num);
		}
	}

	renderPage(num) {
		this.pageRendering = true;
		// Using promise to fetch the page
		this.pdfDoc.getPage(num).then(page => {
			let viewport = page.getViewport(this.scale);

			this.canvas = document.getElementById('the-canvas');
			this.ctx = this.canvas.getContext('2d');

			this.canvas.height = viewport.height;
			this.canvas.width = viewport.width;

			// Render PDF page into canvas context
			let renderContext = {
				canvasContext: this.ctx,
				viewport: viewport
			};
			let renderTask = page.render(renderContext);

			// Wait for rendering to finish
			renderTask.promise.then(result => {
				this.pageRendering = false;
				if (this.pageNumPending !== null) {
					// New page rendering is pending
					this.renderPage(this.pageNumPending);
					this.pageNumPending = null;
				}
			});
		});

		// Update page counters
		//document.getElementById('page_num').textContent = pageNum;
	}
}
