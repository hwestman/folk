import { inject, NewInstance } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';

import * as toastr from 'toastr';

import { ProfileModel } from './ProfileModel';

@inject(HttpClient, Router,  json)
export class ViewProfile {

	url = 'api/profile';

	constructor(http, router) {

		this.router = router;
		this.http = http;
		this.toastr = toastr;
		//this.toastr.options.positionClass = 'toast-top-center';

		this.profile = new ProfileModel();

	}
	activate(params) {
		return this.http.fetch(this.url + '?profileId=' + params.profileId)
			.then(response => response.json())
			.then(data => { // This is a profilemodel, need to exchange field by field to preserve validation
				this.profile.firstname = data.firstname;
				this.profile.lastname = data.lastname;
				this.profile.email = data.email;
				this.profile.gender = data.gender;
				if(data.phone != null){
					this.profile.phone = data.phone.toString();
				}
				this.profile.address = data.address;
				this.profile.city = data.city;
				this.profile.postcode = data.postcode;
				this.profile.picture = data.picture;
				this.profile.role = data.role;
			});
	}
}
export class SingleImgValueConverter {
	toView(fileList) {
		let files = [];
		if (!fileList) {
			return files;
		}

		if (typeof fileList === 'string') {
			return fileList;
		}

		let file = fileList.item(0);
		return URL.createObjectURL(file);
	}
}
export class BlobToUrlValueConverter {
	toView(blob) {
		return URL.createObjectURL(blob);
	}
}

export class FileListToArrayValueConverter {
	toView(fileList) {
		let files = [];
		if (!fileList) {
			return files;
		}
		for (let i = 0; i < fileList.length; i++) {
			files.push(fileList.item(i));
		}
		return files;
	}
}

// export class FileListToArrayValueConverter {
//   toView(fileList) {
//     let files = [];
//     if (!fileList) {
//       return files;
//     }
//     // for(let i = 0; i < fileList.length; i++) {
//     //   files.push(fileList.item(i));
//     // }
//     return fileList.item(0);
//   }
// }
