import { inject, NewInstance } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';

import * as toastr from 'toastr';

import { ValidationController, validateTrigger } from 'aurelia-validation';
import { BootstrapFormRenderer } from '../ui/validation-renderer';
import { ProfileModel } from './ProfileModel';

@inject(HttpClient, Router, NewInstance.of(ValidationController), json)
export class EditProfile {

	url = 'api/profile';

	constructor(http, router, validationController) {

		this.router = router;
		this.http = http;
		this.toastr = toastr;
		//this.toastr.options.positionClass = 'toast-top-center';

		this.profile = new ProfileModel();

		this.validationController = validationController;
		this.validationController.validateTrigger = validateTrigger.change;
		this.validationController.addRenderer(new BootstrapFormRenderer());


	}
	activate() {
		return this.http.fetch(this.url)
			.then(response => response.json())
			.then(data => { // This is a profilemodel, need to exchange field by field to preserve validation
				
				this.profile.firstname = data.firstname;
				this.profile.lastname = data.lastname;
				this.profile.email = data.email;
				this.profile.gender = data.gender;
				if(data.phone != null){
					this.profile.phone = data.phone.toString();
				}
				this.profile.address = data.address;
				this.profile.city = data.city;
				this.profile.postcode = data.postcode;
				this.profile.picture = data.picture;
				this.profile.role = data.role;
			});

		// return this.http.fetch(this.url)
		//         .then(response =>  response.json())
		//         .then(c => this.user = c);
	}
	updateProfile() {
		
		this.validationController.validate().then(result => {
			if (result.length > 0) {
				return;
			}
			this.router.isNavigating = true;
			let profileDTO = {
				firstname: this.profile.firstname,
				lastname: this.profile.lastname,
				phone: this.profile.phone,
				address: this.profile.address,
				postcode: this.profile.postcode,
				city: this.profile.city,
				gender: this.profile.gender,
				role: this.profile.role,
				email: this.profile.email
			};

			let form = new FormData();
			if (this.profile.picture !== null) {
				form.append('file', this.profile.picture[0]);
			}

			form.append('profile', JSON.stringify(profileDTO));

			return this.http.fetch(this.url + '/update', {
				method: 'post',
				body: form
			})
				.then(response => response.json())
				.then(data => {
					this.router.isNavigating = false;
					this.toastr.success('Updated');
				});
		});
	}
}
export class SingleImgValueConverter {
	toView(fileList) {
		let files = [];
		if (!fileList) {
			return files;
		}

		if (typeof fileList === 'string') {
			return fileList;
		}

		let file = fileList.item(0);
		return URL.createObjectURL(file);
	}
}
export class BlobToUrlValueConverter {
	toView(blob) {
		return URL.createObjectURL(blob);
	}
}

export class FileListToArrayValueConverter {
	toView(fileList) {
		let files = [];
		if (!fileList) {
			return files;
		}
		for (let i = 0; i < fileList.length; i++) {
			files.push(fileList.item(i));
		}
		return files;
	}
}

// export class FileListToArrayValueConverter {
//   toView(fileList) {
//     let files = [];
//     if (!fileList) {
//       return files;
//     }
//     // for(let i = 0; i < fileList.length; i++) {
//     //   files.push(fileList.item(i));
//     // }
//     return fileList.item(0);
//   }
// }
