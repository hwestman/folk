import { required, satisfies, email, matches, ValidationRules } from 'aurelia-validation';

export class ProfileModel {
	firstname = '';
	lastname = '';
	picture = '';
	gender = '';
	email = '';
	phone = '';
	address = '';
	city = '';
	postcoce = '';
}


ValidationRules
	.ensure(a => a.firstname).minLength(3)
	.ensure(a => a.lastname).minLength(3)
	.ensure(a => a.email).email()
	.ensure(a => a.postcode).matches(/^[0-9]*$/).withMessage('Numbers only')
	.ensure(a => a.phone)
		.matches(/^[0-9]*$/).withMessage('Numbers only')
		.then()
		.minLength(6)
		.maxLength(10)		
	.on(ProfileModel);


