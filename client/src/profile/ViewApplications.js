import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';
import { DialogService } from 'aurelia-dialog'
import { ViewApplication } from 'profile/ViewApplication';


@inject(HttpClient, Router, DialogService, json)
export class ViewApplications {

	applications = null;
	url = 'api/profile';

	constructor(http, router, dialogService) {

		this.dialogService = dialogService;
		this.router = router;
		this.http = http;		
	};

	activate() {
		return this.http.fetch(this.url + '/applications')
			.then(response => response.json())
			.then(data => {				
				this.applications = data.applications;				
			});
	}
	viewApplication(index, applications) {
		this.dialogService.open({ viewModel: ViewApplication, model: { applications: applications, index: index } })
			.then(result => {
				if (result.wasCancelled) {
					
				}
			});
	}
	// getJob(job) {
	// 	this.router.navigate("jobs/job/" + job._id);
	// }
	// createJob() {
	// 	this.dialogService.open({ viewModel: CreateJobs })
	// 		.then(result => {

	// 			if (!result.wasCancelled) {
	// 				//this.jobs.push(result.output);
	// 				this.activate();
	// 			}
	// 		})
	// }
}