import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import AppRouterConfig from './router-config';
import { FetchConfig } from 'aurelia-auth';
import { AuthService } from 'aurelia-auth';

@inject(Router, FetchConfig, AppRouterConfig,AuthService)
export class Shell {
	constructor(router, fetchConfig, appRouterConfig,auth) {
		this.auth = auth;
		this.router = router;
		this.appRouterConfig = appRouterConfig;
		this.fetchConfig = fetchConfig;

	}
	activate() {
		this.appRouterConfig.configure();
		this.fetchConfig.configure();
	}
	attached() {
		
	}
	get isAuthenticated() {
		return this.auth.isAuthenticated();
	}
}
