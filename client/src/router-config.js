
import { AuthorizeStep } from 'aurelia-auth';
import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';

@inject(Router)
export default class {

	constructor(router) {
		this.router = router;
	}
	configure() {
		let appRouterConfig = function (config) {

			config.title = 'Folk';
			config.addPipelineStep('authorize', AuthorizeStep); // Add a route filter to the authorize extensibility point.

			config.map([
				{ route: ['', 'welcome'], moduleId: './welcome', nav: true, title: 'Welcome' },
				{ route: 'signup', moduleId: './auth/signup', nav: false, title: 'Signup' },
				{ route: 'login', moduleId: './auth/login', nav: false, title: 'Login' },
				{ route: 'logout', moduleId: './auth/logout', nav: false, title: 'Logout' },
				{ route: 'jobs/job/:jobId', moduleId: './jobs/Job', nav: false, title: 'Job', auth: false },
				{ route: 'jobs/updatejob/:jobId', moduleId: './jobs/UpdateJob', nav: false, title: 'Job', auth: true },
				{ route: 'jobs/applyjob/:jobId', moduleId: './jobs/ApplyJob', nav: false, title: 'applyjob', auth: true },
				{ route: 'jobs/listjobs', moduleId: './jobs/ListJobs', nav: true, title: 'Jobs', auth: false },
				{ route: 'jobs/createjob', moduleId: './jobs/CreateJob', nav: true, title: 'CreateJob', auth: true },
				{ route: 'profile/me', moduleId: './profile/EditProfile', nav: true, title: 'Profile', auth: true },
				{ route: 'profile/applications', moduleId: './profile/ViewApplications', nav: true, title: 'Applications', auth: true },
				{ route: 'profile/:profileId', moduleId: './profile/ViewProfile', nav: false, title: 'ViewProfile', auth: false },
				{ route: 'employees', moduleId: './employees', nav: true, title: 'Employees', auth: true },
				{ route: 'onboarding', moduleId: './onboarding/Onboarding', nav: true, title: 'Onboarding', auth: true },
			]);
		};
		this.router.configure(appRouterConfig);
	}

}
