import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';



@inject(HttpClient, Router, json)
export class ViewApplications {

	
	url = 'api/onboarding';

	constructor(http, router) {
		this.router = router;
		this.http = http;		
	};

	activate() {
		
	}
	
}