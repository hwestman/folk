import 'bootstrap';

import config from './auth/auth-config';
import 'whatwg-fetch';

export function configure(aurelia) {
	aurelia.use
		.standardConfiguration()
		.developmentLogging()
		.plugin('aurelia-validation')
		.plugin('aurelia-dialog')
		.plugin('aurelia-auth', (baseConfig) => {
			baseConfig.configure(config);
		});

	aurelia.start().then(a => a.setRoot('shell'));
}
