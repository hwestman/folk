import { inject } from 'aurelia-framework';
@inject(Element)
export class Sidebar {
	constructor(element) {
		this.element = element;
	}

	activate() {
		$("#sidebar").offcanvas({ autohide: true })
	}

	attached() {

		// this.element.addEventListener('click', function(){
		// 	$('#sidebar').offcanvas('hide');
		// });
	}

	detached() {

	}
}