import moment from 'moment';

export class DateFormatValueConverter {
	toView(value) {
		return moment(value).format('D/M/YYYY');
	}
}

export class FilterValueConverter {
    toView(items, search) {
        if(search === "" || search === undefined) return items;

        return items.filter((item) => item["title"].toLowerCase().includes(search.toLowerCase()));
    }
}
