import { inject } from 'aurelia-framework';

import 'bootstrap-select';
const defaultOptions = {
	size: 5,
	liveSearch: true
};

@inject(Element)
export class BootstrapSelectCustomAttribute {
	constructor(element) {
		this.element = element;
	}

	attached() {
		//var self = this;
		let options = Object.assign({}, defaultOptions, this.value || {});
		$(this.element).selectpicker(options);
	}

	detached() {
		$(this.element).selectpicker('destroy');
	}
}
