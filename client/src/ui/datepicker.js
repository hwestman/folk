import { inject, customAttribute } from 'aurelia-framework';
import 'bootstrap-datepicker';

@customAttribute('datepicker')
@inject(Element)
export class DatePicker {
	constructor(element) {
		this.element = element;
	}

	attached() {
		$(this.element).datepicker({ startDate: '+1d', autoclose: true, orientation: 'auto', todayHighlight: true })
			.on('change', e => {
				fireEvent(e.target, 'input');
				//$(this.element).datepicker('hide');
			});
	}

	detached() {
		$(this.element).datepicker('destroy')
			.off('change');
	}
}

function createEvent(name) {
	let event = document.createEvent('Event');
	event.initEvent(name, true, true);
	return event;
}

function fireEvent(element, name) {
	let event = createEvent(name);
	element.dispatchEvent(event);
}
