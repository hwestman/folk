
import { inject, NewInstance } from 'aurelia-dependency-injection';
import { ValidationController, validateTrigger } from 'aurelia-validation';
// import {inject} from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { DialogController } from 'aurelia-dialog'
import { Router } from 'aurelia-router';

import * as toastr from 'toastr';
import { BootstrapFormRenderer } from '../ui/validation-renderer';
import { AJob } from './JobModel';

@inject(HttpClient, Router, NewInstance.of(ValidationController), DialogController, json)
export class CreateJobs {
	findByDate = null;
	selectedManager = null;
	selectedRecruiter = null;
	managers = null;
	url = 'api/job';
	
	constructor(http, router, validationController, dialogController) {
		this.dialogController = dialogController;
		this.router = router;
		this.http = http;

		this.job = new AJob();

		this.validationController = validationController;
		this.validationController.validateTrigger = validateTrigger.change;
		this.validationController.addRenderer(new BootstrapFormRenderer());

		this.toastr = toastr;
		//this.toastr.options.positionClass = 'toast-top-center';
	}

	activate() {
		return this.http.fetch(this.url + '/getCreateData')
			.then(response => response.json())
			.then(data => {
				this.managers = data;
				this.recruiters = data;
			});
	}
	createJob() {
		this.validationController.validate().then(result => {
			if (result.length > 0) {
				return;
			}
			this.router.isNavigating = true;

			this.http.fetch(this.url + '/create', {
				method: 'post',
				body: json(this.job)
			})
				.then(response => {
					this.router.isNavigating = false;
					
					if (response.status === 200) {
						this.toastr.success('Job created');
						//return this.router.navigate('jobs/listjobs');
						return this.dialogController.ok(this.job);

					}
					return response.text().then(text => {
						this.toastr.error(text);
					});
				});
		});
	}
	cancel() {
		this.dialogController.cancel();
	}

}