import { inject, NewInstance } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';
import { DialogController } from 'aurelia-dialog'

import * as toastr from 'toastr';

import { ValidationController, validateTrigger } from 'aurelia-validation';
import { BootstrapFormRenderer } from '../ui/validation-renderer';
import { Application } from './ApplicationModel';

@inject(HttpClient, Router, NewInstance.of(ValidationController),DialogController,json)
export class ApplyJob {

	_isAuthenticated = false;
	url = 'api/job';

	constructor(http, router,validationController,dialogController) {
		this.toastr = toastr;
		this.router = router;
		this.http = http;
		
		//this.toastr.options.positionClass = 'toast-top-center';

		this.dialogController = dialogController;

		this.application = new Application();

		this.validationController = validationController;
		this.validationController.validateTrigger = validateTrigger.change;
		this.validationController.addRenderer(new BootstrapFormRenderer());

		
	}
	activate(job) {
		this.http.fetch('api/profile')
			.then(response => response.json())
			.then(data => {
				this.profile = data;

				this.job = job;
			});
	}
	submitApplication() {
		this.validationController.validate().then(result => {
			if (result.length > 0) {
				return;
			}
			this.router.isNavigating = true;

			if (this.application === null) {
				this.router.isNavigating = false;
				this.toastr.error('Oops, you missed something...');

				return;
			}
			let applicationDTO = {
				profileId: this.profile._id,
				jobId: this.job._id,
				applicationtext: this.application.text
			};
			let applicationForm = new FormData();
			if (this.application.resume != null) {
				applicationForm.append('file', this.application.resume[0]);
			}
			applicationForm.append('application', JSON.stringify(applicationDTO));


			this.http.fetch('api/job/submitapplication', {
				method: 'post',
				body: applicationForm
			})
				.then(data => {
					this.router.isNavigating = false;
					if (data.ok) {
						this.toastr.success('Application Recieved');
						return this.dialogController.ok();
					} else {
						this.toastr.info('No go!');
					}
				}).catch(err => {
					console.log(err);
					this.toastr.info('Error occured, please try again');
				});
		});
	}
	cancel() {
		this.dialogController.cancel();
	}
}
