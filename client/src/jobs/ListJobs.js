import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';
import { DialogService } from 'aurelia-dialog'
import { CreateJobs } from 'jobs/CreateJob'

@inject(HttpClient, Router, DialogService, json)
export class ListJobs {

	jobs = null;
	url = 'api/job';

	constructor(http, router, dialogService) {

		this.dialogService = dialogService;
		this.router = router;
		this.http = http;
		this.allJobs = null;
		

	};

	activate() {
		return this.http.fetch(this.url + '/getJobs')
			.then(response => response.json())
			.then(data => {

				if (data.length > 0) {
					this.jobs = data;
				} else {
					this.jobs = [];
				}
				this.allJobs = this.jobs;

			});
	}
	getJob(job) {
		this.router.navigate("jobs/job/" + job._id);
	}
	createJob() {
		this.dialogService.open({ viewModel: CreateJobs })
			.then(result => {

				if (!result.wasCancelled) {
					//this.jobs.push(result.output);
					this.activate();
				}
			})
	}
}