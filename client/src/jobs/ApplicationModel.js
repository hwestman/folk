import { required, satisfies, email, ValidationRules } from 'aurelia-validation';

export class Application {
	text = '';
	resume = '';
}

ValidationRules
	.ensure(a => a.text).required()
	.on(Application);


