import { required, satisfies, email, ValidationRules } from 'aurelia-validation';

export class Deadline {
	visible = '';
	fillby = '';
}

export class AJob {
	title = '';
	location = '';
	deadline = new Deadline();
	manager = '';
	recruiter = '';
	description = '';
}

ValidationRules.customRule(
	'date',
	(value, obj) => value === null || value === undefined || value instanceof Date,
	'${$displayName} must be a Date.'
);

ValidationRules
	.ensure(a => a.title).required()
	.ensure(a => a.location).required()
	// .ensure(a => a.deadline.visible).required()
	// .ensure(a => a.deadline.fillby).required()

	.ensure(a => a.manager).required()
	.ensure(a => a.recruiter).required()
	.ensure(a => a.description).required()
	.on(AJob);

ValidationRules
	.ensure(a => a.fillby)
	.required()
	.withMessage('Internal fill by date is required')
	.ensure(a => a.visible)
	.required()
	.withMessage('Visible close date is required')
	.on(Deadline);

