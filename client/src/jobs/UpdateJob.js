import { inject,NewInstance } from 'aurelia-framework';
import { ValidationController, validateTrigger } from 'aurelia-validation';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';
import { DialogController } from 'aurelia-dialog';

import * as toastr from 'toastr';

import { BootstrapFormRenderer } from '../ui/validation-renderer';
import { AJob } from './JobModel';

@inject(HttpClient, Router, DialogController,NewInstance.of(ValidationController), json)
export class UpdateJob {
	findByDate = null;
	selectedManager = null;
	selectedRecruiter = null;

	managers = null;
	url = 'api/job';

	constructor(http, router, dialogController,validationController) {

		this.dialogController = dialogController;
		
		this.router = router;
		this.http = http;

		this.toastr = toastr;
		//this.toastr.options.positionClass = 'toast-top-center';

		
		this.validationController = validationController;
		this.validationController.validateTrigger = validateTrigger.change;
		this.validationController.addRenderer(new BootstrapFormRenderer());

		this.job = new AJob();
		
	}
	activate(job) {
		
		this.job._id = job._id;
		this.job.deadline = job.deadline;
		this.job.description = job.description;
		this.job.location = job.location;
		this.job.manager = job.manager;
		this.job.recruiter = job.recruiter;
		this.job.title = job.title;
		return this.http.fetch(this.url + '/getUpdateData', {
			method: 'post',
			body: json({ jobId: job._id })

		}).then(response => response.json())
			.then(data => {
				
				this.managers = data.managers;
				this.recruiters = data.recruiters;
				
				// this.selectedRecruiter = this.recruiters.findIndex(x => x._id == this.job.recruiter.id);
			});
	}
	updateAJob(jobId, routeConfig) {

	}

	deleteJob(jobId, routeConfig) {
		this.router.isNavigating = true;
		if (confirm('Are you sure you want to delete?')) {
			this.http.fetch(this.url + '/delete', {
				method: 'post',
				body: json({ jobId: jobId })
			})
				.then(response => {
					this.router.isNavigating = false;
					if (response.ok) {
						this.toastr.success("Job deleted");
						this.dialogController.ok();
						this.router.navigate('jobs/listjobs/');
					} else {
						this.toastr.error(response.statusText);
					}

				});
		} else {
			this.router.isNavigating = false;
		}
	}
	save() {
		this.validationController.validate().then(result => {
			if (result.length > 0) {
				return;
			}
			this.router.isNavigating = true;
			let jobDTO = {
				jobId: this.job._id,
				title: this.job.title,
				deadline: this.job.deadline,
				manager: this.job.manager,
				recruiter: this.job.recruiter,
				description: this.job.description,
				location: this.job.location
			};

			this.http.fetch(this.url + '/update', {
				method: 'post',
				body: json(jobDTO)
			})
				.then(response => {
					this.router.isNavigating = false;
					if (response.ok) {
						this.toastr.success('Updated');
						this.dialogController.ok();
					} else {
						this.toastr.error(response.statusText);
					}

					// this.router.navigate("jobs/job/"+this.job._id);
				});
		});
	}
	cancel() {
		this.dialogController.cancel();
	}
}
