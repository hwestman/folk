import { inject } from 'aurelia-framework';
import { HttpClient, json } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';
import { AuthService } from 'aurelia-auth';
import { DialogService } from 'aurelia-dialog';
import { UpdateJob } from 'jobs/UpdateJob';
import { ApplyJob } from 'jobs/ApplyJob';
import { ViewApplication } from 'jobs/ViewApplication';

@inject(HttpClient, Router, AuthService, DialogService, json)
export class Job {

	url = 'api/job';
	constructor(http, router, auth, dialogService) {
		this.dialogService = dialogService;
		this.auth = auth;
		this.router = router;
		this.http = http;
		this.params = null;		
	}
	activate(params) {
		this.params = params;
		return this.auth.getMe().then(currentUser => {
			this.currentUser = currentUser._id;
		
			this.http.fetch(this.url + '/job?jobId=' + params.jobId)
				.then(response => response.json())
				.then(data => {
					this.applications = data.applications;
					this.job = data.job;
					if (this.applications.length > 0 && this.applications.find(x => x.userProfile.userId == this.currentUser) != null) {
						this.canApply = false;
					} else {
						this.canApply = true;
					}
				});
		});
	}

	editJob() {
		let original = JSON.parse(JSON.stringify(this.job));
		this.dialogService.open({ viewModel: UpdateJob, model: this.job })
			.then(result => {
				if (result.wasCancelled) {
					this.job = original;
				}
			});

		// this.router.navigate('jobs/updateJob/' + job);
	}
	applyJob(job) {
		if (this.canApply) {
			this.dialogService.open({ viewModel: ApplyJob, model: this.job })
				.then(result => {
					if (!result.wasCancelled) {
						//this.jobs.push(result.output);
						this.activate(this.params);
					}
				});
		}
		return;
	}
	viewApplication(index, applications) {
		this.dialogService.open({ viewModel: ViewApplication, model: { applications: applications, index: index } })
			.then(result => {
				if (result.wasCancelled) {
					
				}
			});
	}
	viewProfile(userId){
		this.router.navigate("profile/" + userId);
	}
}
