import { required, satisfies, email, ValidationRules } from 'aurelia-validation';

export class SignupForm {
	firstname = '';
	lastname = '';
	email = '';
	password = '';
}

ValidationRules
	.ensure(a => a.firstname).required().minLength(3)
	.ensure(a => a.lastname).required().minLength(3)
	.ensure(a => a.email).required().email()
	.ensure(a => a.password).required().minLength(4)
	.on(SignupForm);
