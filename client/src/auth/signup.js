import { AuthService } from 'aurelia-auth';
import { inject, NewInstance } from 'aurelia-dependency-injection';
import { ValidationController, validateTrigger } from 'aurelia-validation';
import { Router } from 'aurelia-router';

import { BootstrapFormRenderer } from '../ui/validation-renderer';
import { SignupForm } from './SignupModel';
import * as toastr from 'toastr';

@inject(AuthService, NewInstance.of(ValidationController), Router)
export class Signup {
	constructor(auth, controller, router) {
		this.auth = auth;
		this.router = router;
		this.signupForm = new SignupForm();
		this.controller = controller;
		this.controller.addRenderer(new BootstrapFormRenderer());

		this.toastr = toastr;
		//this.toastr.options.positionClass = 'toast-top-center';
	}
	heading = 'Sign Up';

	signup() {
		this.router.isNavigating = true;
		this.controller.validate().then(result => {
			if (result.length > 0) {
				return;
			}
			return this.auth.signup(this.signupForm.firstname + '_' + this.signupForm.lastname, this.signupForm.email, this.signupForm.password)
				.then((response) => {
					this.router.isNavigating = false;
					this.toastr.success('Signed up and logged in :)');
				}).catch(error => {
					this.router.isNavigating = false;
					return error.text().then(text => {
						this.toastr.error(text);
					});
				});
		});
	}
}
