import { AuthService } from 'aurelia-auth';
import { inject,NewInstance } from 'aurelia-framework';
import { ValidationController, validateTrigger } from 'aurelia-validation';
import { Router } from 'aurelia-router';
import * as toastr from 'toastr';

import { SignupForm } from './SignupModel';
import { BootstrapFormRenderer } from '../ui/validation-renderer';

@inject(AuthService, NewInstance.of(ValidationController),  Router)
export class Login {
	constructor(auth, validationController,router) {
		this.validationController = validationController;
		this.router = router;
		this.auth = auth;
		this.toastr = toastr;
		// this.toastr.options.positionClass = 'toast-top-center';

		this.validationController.addRenderer(new BootstrapFormRenderer());
		this.signupForm = new SignupForm();
	}
	heading = 'Login';

	email = '';
	password = '';
	login() {
		this.router.isNavigating = true;
		return this.auth.login(this.email, this.password)
			.then(response => {
				this.router.isNavigating = false;
				
				this.toastr.success('Logged in!');
			})
			.catch(err => {
				this.router.isNavigating = false;
				this.toastr.error('Wrong username or password');
			});
	}

	authenticate(name) {
		return this.auth.authenticate(name, false, null)
			.then((response) => {
				console.log(response);
			});
	}
	signup() {
		this.router.isNavigating = true;
		this.validationController.validate().then(result => {
			if (result.length > 0) {
				return;
			}
			return this.auth.signup(this.signupForm.firstname + '_' + this.signupForm.lastname, this.signupForm.email, this.signupForm.password)
				.then((response) => {
					this.router.isNavigating = false;
					this.toastr.success('Signed up and logged in :)');
				}).catch(error => {
					this.router.isNavigating = false;
					return error.text().then(text => {
						this.toastr.error(text);
					});
				});
		});
	}
}
