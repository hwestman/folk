let configForDevelopment = {
	signupRoute: '/login',
	providers: {
		google: {
			//responseType :'code',
			clientId: '12874121084-hfodvndj3b1kdemrf9s787vqnvhkv06r.apps.googleusercontent.com',
			state: function(){
                var val = ((Date.now() + Math.random()) * Math.random()).toString().replace(".", "");
                return encodeURIComponent(val);
            }
		},
		
		linkedin: {
			clientId: ''
		},
		facebook: {
			clientId: ''
		}
	}
};

let configForProduction = {
	providers: {
		google: {
			//responseType :'code',
			clientId: '12874121084-hfodvndj3b1kdemrf9s787vqnvhkv06r.apps.googleusercontent.com',
			state: function(){
                var val = ((Date.now() + Math.random()) * Math.random()).toString().replace(".", "");
                return encodeURIComponent(val);
            }
		},
		linkedin: {
			clientId: ''
		},
		facebook: {
			clientId: ''
		}

	}
};
let config;
if (window.location.hostname === 'localhost') {
	config = configForDevelopment;
} else {
	config = configForProduction;
}


export default config;
