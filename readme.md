# Folk

**MEAN application for managing a recruitment process**

- Mongo
- Express
- Aurelia
- NodeJS 

A demo is found here: https://folk.westman.no

Sign up manually or with google account, use admin/admin for (you guessed it) admin access.

## Docker
**Build**
`docker build -t folk .`

Make sure you run a mongo instance with alias db on standard mongo port (folk app is connecting to db:27017)

**Run**
`docker run -d --name folk -p 1337:1337 -t folk`


## Set up environment
----

Make sure you have installed..
* node.js (https://nodejs.org/en/)
* npm (should come with node)
* git (https://git-scm.com/)
* Mongodb

### Clone this repo

```git clone git@gitlab.com:hwestman/folk.git```

**This is the commands needed for first time install:**
```
cd folk/server && sudo npm install
cd ../client && sudo npm install
sudo npm install jspm --g
sudo jspm install

//In case gulp wont install with npm install run the next one
sudo npm install -g gulp

gulp build 

```
If you are running mongodb as a service skip the next one

```
//Open up terminal and run
mongod
```

```
cd ../server && node server.js
//Should be up and running
```

### Recurring build should just do this:
------
```
git pull
cd folk/server && sudo npm install
cd ../client && sudo npm install
sudo jspm install
gulp watch 
cd ../server && node server.js
```







